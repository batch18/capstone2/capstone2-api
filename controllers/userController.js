const User = require('./../models/User');
const Order = require('./../models/Orders');
const Product = require('./../models/Product');
const bcrypt = require('bcrypt');
const auth = require('./../auth');

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then((result) => {
		if(result.length != 0){
			return true
		} else {
			return false
		}
	});
}


module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		address: reqBody.address,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then( (error, result) => {
		if(error){
			return error
		} else {
			return true
		}
	});
}

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then((result) => {
		if(result == null){
			return false
		} 
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect === true) {
				return { access : auth.createAccessToken(result.toObject())}
			} 
			else {
				return false
			} 

		}
	})
}

module.exports.getProfile = (data) => {
	return User.findById({_id: data}).then((result) => {
		result.password = "******"
		return result

	})
}

module.exports.findUser = (params) => {
	return User.findById(params).then( result => {
		return result
	})
}


module.exports.setAdmin = (data, params) => {
	return User.findById({_id: data}).then((result) => {
		if(result.isAdmin === true){

			const update = {isAdmin : true};
			return User.findByIdAndUpdate(params ,update, {new: true})
			.then((result, error) => {
				if(error){
					return error
				} else {
					return result
				}
			})
		}
		else {
			console.log("something went wrong")
		}
	})
}



module.exports.setNotAdmin = (data, params) => {
	return User.findById({_id: data}).then((result) => {
		
		if(result.isAdmin === true){

			const update = {isAdmin : false};
			return User.findByIdAndUpdate(params ,update, {new: true})
			.then((result, error) => {
				if(error){
					return error
				} else {
					return result
				}
			})
		}
		else {
			console.log("something went wrong")
		}
	})
}

module.exports.checkOut = (data) => {
	return User.findById(data.userId).then(user => {
		return Product.findById(data.productId).then( result => { 
			user.cart.push({itemName: result.productName, description: result.description, price: result.price,})
			return user.save().then((user, error) => {
				if (error){
					return false
				}
			});
		})	
	})
}
	

module.exports.purchase = (data) => {
	return User.findById(data.userId).then(user => {
		return Product.findById(data.productId).then( result1 => {
			let newOrder = new Order({
				price: result1.price,
				customerName: data.userId,
				customerAddress : user.address,
				itemsPurchased: [{
					productName: result1.productName,
					description: result1.description,
					price: result1.price
				}]
			}); 

			user.history.push({itemName: result1.productName, description: result1.description, price: result1.price,})
			return user.save().then((user, error) => {
				if (error){

					return false
				}
				else {
					
					return newOrder.save().then( (result, error) => {
						if(error){
								return error
							}
							else {
								
									
								let updatedProduct = {
									inventory: result1.inventory - 1,
								};


								return Product.findByIdAndUpdate(data.productId, updatedProduct, {new: true})

								.then((result1, error) => {
									if(error){
										return error
									} else {
										return result1
									}
								})
									
									
								
							}
					});
				}
			});
		})	
	})
}


			
module.exports.purchaseFromCart = (data) => {
	return User.findById(data.userId).then(user => {
		return Product.findById(data.productId).then( result1 => {
			let newOrder = new Order({
				price: result1.price,
				customerName: data.userId,
				customerAddress : user.address,
				itemsPurchased: [{
					productName: result1.productName,
					description: result1.description,
					price: result1.price
				}]
			}); 

			user.history.push({itemName: result1.productName, description: result1.description, price: result1.price,})
			return user.save().then((user, error) => {
				if (error){

					return false
				}
				else {
					
					return newOrder.save().then( (result, error) => {
						if(error){
								return error
							}
							else {
								
									
								let updatedProduct = {
									inventory: result1.inventory - 1,
								};


								return Product.findByIdAndUpdate(data.productId, updatedProduct, {new: true})

								.then((result1, error) => {
									if(error){
										return error
									} else {
										return result1
									}
								})
									
									
								
							}
					});
				}
			});
		})	
	})
}



module.exports.cartTotal = (data) => {
	return User.findById({_id: data}).then((result) => {
		let sum = 0
			for (let i = 0; i < result.cart.length; i++) {
				sum = sum + Number(result.cart[i].price);			
			}
		return sum
	})
}