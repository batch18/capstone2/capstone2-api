const Product = require('./../models/Product')
const User = require('./../models/User');

module.exports.addProduct = (data, reqBody) => {
	return User.findById({_id: data}).then((result) => {
		console.log(result.isAdmin)
		if(result.isAdmin === true){
			let newProduct = new Product({
				productName: reqBody.productName,
				description: reqBody.description,
				price: reqBody.price,
				inventory : reqBody.inventory
			});


			return newProduct.save().then( (error, result) => {
				if(error){
					return error
				} else {
					return newProduct
				}
			});
		}
		else {
			console.log("something went wrong")
		}
	})
}


module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then( result => {
			for (let i = 0; i < result.length; i++) {
				if(result[i].inventory <= 0){
					const update = {isActive : false};
					return Product.findByIdAndUpdate(result[i]._id ,update, {new: true})
					.then((result, error) => {
						if(error){
							return error
						} else {
							return result
						}
					})
				}
				else {
					console.log("something went wrong")
				}	
			}
		return result
	})
}

module.exports.getAllInactive = () => {
	return Product.find({isActive : false}).then( result => {
		for (let i = 0; i < result.length; i++) {
			if(result[i].inventory > 0){
				const update = {isActive : true};
				return Product.findByIdAndUpdate(result[i]._id ,update, {new: true})
				.then((result, error) => {
					if(error){
						return error
					} else {
						return result
					}
				})
			}
			else {
				console.log("something went wrong")
			}	
		}
		return result
	})
}

module.exports.all = () => {
	return Product.find().then( result => {
		return result
	})
}

module.exports.findProduct = (params) => {
	return Product.findById(params).then( result => {
		return result
	})
}


module.exports.editProduct = (data, params, reqBody) => {
	return User.findById({_id: data}).then((result) => {
		console.log(reqBody)
		if(result.isAdmin === true){
			let updatedProduct = {
				productName: reqBody.productName,
				description: reqBody.description,
				price: reqBody.price,
			};


			return Product.findByIdAndUpdate(params, updatedProduct, {new: true})

			.then((result, error) => {
				if(error){
					return error
				} else {
					return result
				}
			})
		}
		else {
			console.log("something went wrong")
		}
	})
}

module.exports.deactivate = (data, params) => {
	return User.findById({_id: data}).then((result) => {

		if(result.isAdmin === true){

			const update = {isActive : false};
			return Product.findByIdAndUpdate(params ,update, {new: true})
			.then((result, error) => {
				if(error){
					return error
				} else {
					return result
				}
			})
		}

		else {
			console.log("something went wrong")
		}
	})
}

module.exports.activate = (data, params) => {
	return User.findById({_id: data}).then((result) => {

		if(result.isAdmin === true){

			const update = {isActive : true};
			return Product.findByIdAndUpdate(params ,update, {new: true})
			.then((result, error) => {
				if(error){
					return error
				} else {
					return result
				}
			})
		}

		else {
			console.log("something went wrong")
		}
	})
}

module.exports.deleteProduct = (data, params) => {
	return User.findById({_id: data}).then((result) => {

		if(result.isAdmin === true){

			return Product.findByIdAndDelete(params)
				.then((result, error) => {
					if(error){
						return error
					} else {
						return result
					}
				})
		}

		else {
			console.log("something went wrong")
		}
	})
}

module.exports.restock = (data, params, reqBody) => {
	return User.findById({_id: data}).then((result1) => {
		return Product.findById({_id: params}).then((result) => {

			let newInventory = result.inventory + reqBody.inventory


			if(result1.isAdmin === true){
				let updatedProduct = {
					inventory: newInventory
				};


				return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
				.then((result, error) => {
					if(error){
						return error
					} else {
						return result
					}
				})
			}
			else {
				console.log("something went wrong")
			}
			
		})
	})
}



/**/