const Order = require('./../models/Orders')
const User = require('./../models/User');
const Reqest = require('./../models/Request')
const Product = require('./../models/Product')

module.exports.allOrders = (data, params) => {
	return User.findById({_id: data}).then((result) => {
		
		if(result.isAdmin === true){
			return Order.find().then( result => {
				return result
			})
		}
		else {
			console.log("something went wrong")
		}
	})
}

module.exports.myOrders = (data) => {
	return User.findById({_id: data}).then((result) => {
	if(result.isAdmin === true){
		console.log("being admin means no cart :(")
	}
	else {	
			
			return result.cart
		}
	})
}

module.exports.purchaseHistory = (data) => {
	return User.findById({_id: data}).then((result) => {
	if(result.isAdmin === true){
		console.log("being admin means no purchase history :(")
	}
	else {	
			
			return result.history
		}
	})
}


module.exports.orders = (data) => {
	return Order.find({customerName: data}).then( result => {
		console.log(result.id)
		return result
	})
}

module.exports.request = (data) => {
	return User.findById(data.userId).then(user => {
		return Product.findById(data.productId).then( result1 => {

			let name = user.firstName +' '+ user.lastName

			let newRequest = new Reqest({
				productId: data.productId,
				customerName: name,
				quantity: reqBody.qty
			});


			return newRequest.save().then( (error, result) => {
				if(error){
					return error
				} else {
					return newRequest
				}
			});
		})	
	})
}