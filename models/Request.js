const mongoose = require('mongoose');

const requestSchema = new mongoose.Schema(
	{
		productId: {
			type: Number,
			required: [true, 'price is required']
		},
		quantity: {
			type: Number,
			required: [true, 'quantity is required']
		},
		customerName: {
			type: String,
			required: [true, 'customer name is required']
		}
	}
);

module.exports = mongoose.model('Request', requestSchema);