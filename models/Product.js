const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
	{
		productName: {
			type: String,
			required: [true, 'product name is required']
		},
		description: {
			type: String,
			required: [true, 'description is required']
		},
		price: {
			type: Number,
			required: [true, 'price is required']
		},
		inventory: {
			type: Number,
			default: 0
		},
		createdOn: {
			type: Date,
				default: new Date()
		},
		isActive: {
			type: Boolean,
			default: true
		}
	}
);

module.exports = mongoose.model('Product', productSchema);