const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
	{
		price: {
			type: Number,
			required: [true, 'price is required']
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		customerName: {
			type: String,
			required: [true, 'customer name is required']
		},
		customerAddress: {
			type: String,
			required: [true, 'customer address is required']
		},
		itemsPurchased: [
		{
			productName: {
				type: String,
				required: [true, 'product name is required']
			},
			description: {
				type: String,
				required: [true, 'description is required']
			},
			price: {
				type: Number,
				required: [true, 'price is required']
			}
		}
		]
	}
);

module.exports = mongoose.model('Orders', orderSchema);