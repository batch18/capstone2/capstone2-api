const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, 'First name is required']
		},
		lastName: {
			type: String,
			required: [true, 'Last name is required']
		},
		address: {
			type: String,
			required: [true, 'Address is required']
		},
		email: {
			type: String,
			required: [true, 'Email is required']
		},
		password: {
			type: String,
			required: [true, 'Password is required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		cartTotal: {
			type: Number,
			default: 0
		},
		cart: [
		{
			itemName: {
				type: String,
				required: [true, 'item name is required']
			},
			description: {
				type: String,
				required: [true, 'description is required']
			},
			price: {
				type: Number,
				required: [true, 'price is required']
			}
		}
		],
		history: [
		{
			itemName: {
				type: String,
				required: [true, 'item name is required']
			},
			description: {
				type: String,
				required: [true, 'description is required']
			},
			price: {
				type: Number,
				required: [true, 'price is required']
			}
		}
		]
	}
);

module.exports = mongoose.model('User', userSchema);