const express = require('express');
const mongoose = require('mongoose');
const port = process.env.PORT || 3000;
const app = express();
const cors = require('cors');

//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://LaFenice:_3Valentine-4@cluster0.znk4w.mongodb.net/capstone-2?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
).then(()=> console.log(`connected to database`)).catch((error)=>console.log(error));

//routes
let userRoutes = require('./routes/userRoutes')
app.use("/api/users", userRoutes);

let productRoutes = require('./routes/productRoutes')
app.use("/api/products", productRoutes);

let orderRoutes = require('./routes/orderRoutes')
app.use("/api/orders", orderRoutes);


app.listen(port, () =>(console.log(`server running on port ${port}`)));