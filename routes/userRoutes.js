const express = require('express');
const router = express.Router();


//controllers
const userController = require('./../controllers/userController');

//auth
const auth = require('./../auth');

//check if email exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(result => res.send(result))
})

//user registration
router.post('/register', (req,res) => {
	userController.register(req.body).then(result => res.send(result))
})

//user login
router.post('/login', (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})

//get user details
router.get('/details', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(result => res.send(result))
})

//get total cart value
router.get('/cartTotal', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.cartTotal(userData.id).then(result => res.send((result).toString()))
})

//find user
router.get('/:userId', auth.verify, (req, res) => {
	userController.findUser(req.params.userId).then(result => res.send(result));
})

//set as admin
router.get('/setAdmin/:userId', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.setAdmin(userData.id, req.params.userId).then(result => res.send(result))
})

//revoke as admin
router.get('/setNotAdmin/:userId', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.setNotAdmin(userData.id, req.params.userId).then(result => res.send(result))
})

// order
router.post('/checkOut', auth.verify,(req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	userController.checkOut(data).then(result => res.send(result))
})

// purchase from catalogue
router.post('/purchase', auth.verify,(req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	userController.purchase(data).then(result => res.send(result))
})

// purchase from catalogue
router.post('/purchaseFromCart', auth.verify,(req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	userController.purchaseFromCart(data).then(result => res.send(result))
})




module.exports = router;