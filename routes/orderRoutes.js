const express = require('express');
const router = express.Router();
const auth = require('./../auth');

const orderController = require ('./../controllers/orderController')

//get all orders
router.get('/allOrders', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	orderController.allOrders(userData.id).then(result => res.send(result))
})

//get my orders
router.get('/myOrders', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	orderController.myOrders(userData.id).then(result => res.send(result))
})

router.get('/orders', (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	orderController.orders(userData.id).then(result => res.send(result))
})

// purchase history
router.get('/purchaseHistory', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	orderController.purchaseHistory(userData.id).then(result => res.send(result))
})

router.put('/:productId/request', auth.verify,(req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.params.productId
	}
	orderController.request(data, req.body).then(result => res.send(result))
})


module.exports = router;