const express = require('express');
const router = express.Router();
let auth = require('./../auth')

const productController = require ('./../controllers/productController')

//add product
router.post('/addProduct', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	productController.addProduct(userData.id, req.body).then(result => res.send(result))
})

/*router.post('/addProduct', auth.verify, (req, res) => {
	productController.addProduct(req.body).then(result => res.send(result));
})*/

// get active product
router.get('/active', (req, res) => {
	productController.getAllActive().then(result => res.send(result));
})

// get active product
router.get('/inactive', (req, res) => {
	productController.getAllInactive().then(result => res.send(result));
})

router.get('/all', (req, res) => {
	productController.all().then(result => res.send(result));
})

// get single product
router.get('/:productId', auth.verify, (req, res) => {
	productController.findProduct(req.params.productId).then(result => res.send(result));
})

// get single product
router.put('/:productId/edit', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.editProduct(userData.id, req.params.productId, req.body).then(result => res.send(result));
})

// get single product
router.put('/:productId/restock', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.restock(userData.id, req.params.productId, req.body).then(result => res.send(result));
})

// archive product
router.get('/:productId/deactivate', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.deactivate(userData.id, req.params.productId).then(result => res.send(result));
})

// un-archive product
router.get('/:productId/activate', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.activate(userData.id, req.params.productId).then(result => res.send(result));
})

// delete product
router.delete('/:productId/delete', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.deleteProduct(userData.id, req.params.productId).then(result => res.send(result));
})

module.exports = router;